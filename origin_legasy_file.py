import time
import os
import sys
import pygame
import tkinter as tk
from threading import Timer

root = tk.Tk()

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

img_dir = resource_path("pictures")
sound_dir = resource_path("sound")

main_window = tk.Canvas(root, width=600, height=300, background="lightblue") #define window configuration
root.title('RadioKaruna')
titleIcon=tk.PhotoImage(file='pictures/logoRK.png') # define window title icon (WTI)
root.iconphoto(False,titleIcon) #apply WTI
playbutton20x20 = tk.PhotoImage(file="pictures/playbutton20x20.png") #playbutton icon
stopbutton20x20 = tk.PhotoImage(file="pictures/stopbutton20x20.png") #stopbutton icon
karunaimage = tk.PhotoImage(file="pictures/KarunaLogo.png")
main_window.pack()

pygame.init()
clock = pygame.time.Clock()
    #audio files
sound1 = pygame.mixer.Sound('sound/gong.mp3')
bird1 = pygame.mixer.Sound('sound/twit1.wav')
bird2 = pygame.mixer.Sound('sound/twit2.wav')
bird3 = pygame.mixer.Sound('sound/twit3.wav')
bird4 = pygame.mixer.Sound('sound/twit4.wav')


def applyTime(): #set start timer
    label1 = tk.Label(root, text='збережено :)', fg='blue', bg="lightblue", font=('Arial Bold', 8, 'bold'))
    main_window.create_window(300, 270, window=label1)
    from datetime import datetime
    current_time = datetime.now()

    cur_hour = int(current_time.hour)
    cur_min = int(current_time.minute)
    spStartHour = int(hourStart.get())
    spStartMin = int(minutStart.get())
    spStopHour = int(hourStop.get())
    spStopMin = int(minutStop.get())
    spGapMin = int(minutGap.get())

    print("Поточний час: ",cur_hour,':',cur_min)
    print("Початок роботи: ",spStartHour,':',spStartMin)
    print("Кінець роботи:  ",spStopHour,':',spStopMin)
    print("Гонг, що ",spGapMin, "хв.")
    return (spStartHour,spStartMin,spStopHour,spStopMin,spGapMin)

def timeCheck():
    from datetime import datetime
    current_time = datetime.now()
    cur_hour = int(current_time.hour)
    cur_min = int(current_time.minute)
    spStartHour = int(hourStart.get())
    spStartMin = int(minutStart.get())
    spStopHour = int(hourStop.get())
    spStopMin = int(minutStop.get())
    spGapMin = int(minutGap.get())
    if cur_hour*60 + cur_min >= spStartHour*60 + spStartMin and cur_hour*60 + cur_min <= spStopHour*60 + spStopMin:
        a = True
    else:
        a = False

    print(a)
    print((spStartHour*60 + spStartMin),(cur_hour*60 + cur_min),(spStopHour*60 + spStopMin))

    return a

#Timer(60 audioPlayer).start()
"""def audioPlayer(): #play function
    Timer(30, audioPlayer).start()
#    alert_minutes = [15, 30, 45, 0]
#    now = time.localtime(time.time())
#    if now.tm_min in alert_minutes:
#        pass
    if var0.get() == True:
        if timeCheck() == True:
            sound1.play()
            print("good")
    else:
        print("no good")
        
        """
def audioPlayer():
    i = 1
    audioPlayer2(i)
    return

def audioPlayer2(i):
    if i == True:
        pass
    else:
        return
    if var0.get() == True:
        if timeCheck() == True:
            sound1.play()
            print("good")
        else:
                print("no good")
    Timer(30, audioPlayer2()).start()

    labelRun = tk.Label(root, width=15, text=f'Активних семплів: {(var0.get()+var1.get())+var2.get()+var3.get()+var4.get()}', fg='green', bg="lightblue", font=('Arial Bold', 8, 'bold'))
    main_window.create_window(450, 270, window=labelRun)

    rand = randomBird(var1, var2, var3, var4)
    if rand.get() == True and rand == var1:
        bird1.play()
        print('now played:', rand, bird1.get_length())

    elif rand.get() == True and rand == var2:
        bird2.play()
        print('now played:', rand, bird2.get_length())
    elif rand.get() == True and rand == var3:
        bird3.play()
        print('now played:', rand, bird3.get_length())
    elif rand.get() == True and rand == var4:
        bird4.play()
        print('now played:', rand, bird4.get_length())
    else:
        print('next roll')
        pass



def allWayStop():
    bird1.stop(), bird2.stop(),bird3.stop(),bird4.stop(),sound1.stop() #stop playing samples
    labelStop = tk.Label(root, width=15, text='Зупинено', fg='brown', bg="lightblue", font=('Arial Bold', 8, 'bold'))
    main_window.create_window(450, 270, window=labelStop)
    i = 0
    audioPlayer2(i)

def randomBird(a,b,c,d): #set stop timer
    import random
    z = random.choice([a,b,c,d])
    return z

#Labels
    #initiation
programStart = tk.Label(root, width=23, text='Початок (перший гонг):', fg='black', bg="lightblue", font=('Arial Bold', 8, 'bold'))
programEnd = tk.Label(root,width=23, text='Кінець (останній гонг):', fg='black', bg="lightblue", font=('Arial Bold', 8, 'bold'))
programGap = tk.Label(root,width=23, text='Періодичність в хвилинах:', fg='black', bg="lightblue", font=('Arial Bold', 8, 'bold'))

dDot1 = tk.Label(root, width=2, text=':', fg='black', bg="lightblue", font=('Arial Bold', 16, 'bold'))
dDot2 = tk.Label(root, width=2, text=':', fg='black', bg="lightblue", font=('Arial Bold', 16, 'bold'))

    #placement
main_window.create_window(150, 175, window=programStart)
main_window.create_window(300, 175, window=programEnd)
main_window.create_window(450, 175, window=programGap)

main_window.create_window(150, 200, window=dDot1)
main_window.create_window(300, 200, window=dDot2)


#Filds
    #initialization
hourStart=tk.Entry(root,font=("Arial Bold",14),width=2,selectbackground="blue")
minutStart=tk.Entry(root,font=("Arial Bold",14),width=2,selectbackground="blue")
hourStop=tk.Entry(root,font=("Arial Bold",14),width=2,selectbackground="yellow")
minutStop=tk.Entry(root,font=("Arial Bold",14),width=2,selectbackground="yellow")
minutGap=tk.Entry(root,font=("Arial Bold",14),width=2)
    #placement
main_window.create_window(130, 200, window=hourStart)
main_window.create_window(170, 200, window=minutStart)
main_window.create_window(280, 200, window=hourStop)
main_window.create_window(320, 200, window=minutStop)
main_window.create_window(450, 200, window=minutGap)
    #set default values
minutGap.focus()

minutGap.insert(1,"30")
hourStart.insert(1,"10")
hourStop.insert(1,"18")
minutStart.insert(1,"00")
minutStop.insert(1,"00")

# Buttons
    #initiation
play0 = tk.Button(image=playbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (sound1.stop(), sound1.play())) # gong playbutton
stop0 = tk.Button(image=stopbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (sound1.stop())) # gong stopbutton

play1 = tk.Button(image=playbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (bird1.stop(), bird1.play())) # twit1 playbutton
stop1 = tk.Button(image=stopbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (bird1.stop())) # twit1 stopbutton
play2 = tk.Button(image=playbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (bird2.stop(), bird2.play())) # twit2 playbutton
stop2 = tk.Button(image=stopbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (bird2.stop())) # twit2 stopbutton
play3 = tk.Button(image=playbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (bird3.stop(), bird3.play())) # twit3 playbutton
stop3 = tk.Button(image=stopbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (bird3.stop())) # twit3 stopbutton
play4 = tk.Button(image=playbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (bird4.stop(), bird4.play())) # twit4 playbutton
stop4 = tk.Button(image=stopbutton20x20, relief="flat",activebackground="lightblue",bg="lightblue",command= lambda : (bird4.stop())) # twit4 stopbutton

applyButton = tk.Button(text='Застосувати', command=applyTime, bg='brown', fg='black')
mainStart = tk.Button(text='Пуск', command=audioPlayer,bg='lightgreen', fg='black')
mainStop = tk.Button(text='Стоп', command=allWayStop, bg='red', fg='black')

    # placement
main_window.create_window(300, 240, window=applyButton)
main_window.create_window(425, 240, window=mainStart)
main_window.create_window(475, 240, window=mainStop)


main_window.create_window(140, 30, window=play0)
main_window.create_window(165, 30, window=stop0)
main_window.create_window(140, 60, window=play1)
main_window.create_window(165, 60, window=stop1)
main_window.create_window(140, 90, window=play2)
main_window.create_window(165, 90, window=stop2)
main_window.create_window(140, 120, window=play3)
main_window.create_window(165, 120, window=stop3)
main_window.create_window(140, 150, window=play4)
main_window.create_window(165, 150, window=stop4)

#checkbox
    #variables for checkbox
var0=tk.IntVar()
var1=tk.IntVar()
var2=tk.IntVar()
var3=tk.IntVar()
var4=tk.IntVar()
    #initialization
check0 = tk.Checkbutton(main_window, text="Гонг      ",width=10,variable=var0, onvalue=1, offvalue=0)
check1 = tk.Checkbutton(main_window, text="Цвіріньк-1",width=10,variable=var1, onvalue=1, offvalue=0)
check2 = tk.Checkbutton(main_window, text="Цвіріньк-2",width=10,variable=var2, onvalue=1, offvalue=0)
check3 = tk.Checkbutton(main_window, text="Цвіріньк-3",width=10,variable=var3, onvalue=1, offvalue=0)
check4 = tk.Checkbutton(main_window, text="Цвіріньк-4",width=10,variable=var4, onvalue=1, offvalue=0)
    #placement
main_window.create_window(60,30,window=check0)
main_window.create_window(60,60,window=check1)
main_window.create_window(60,90,window=check2)
main_window.create_window(60,120,window=check3)
main_window.create_window(60,150,window=check4)
check0.select()

root.mainloop()
pygame.quit()